import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

it('renders without crashing', () => {
  shallow(<App />);
});

const array = [100, 50, 20, 10, 5, 1, .25, .10, .05, .01]

const reduce = (result,currentValue) => {
  result[currentValue] = Math.floor(result.init / currentValue)
  result.init = ((result.init * 100) % (currentValue * 100)) / 100
	return result
}

const handleChangeCalculate = (param = 10) => {
  const result = array.reduce(reduce, { init: param })
  delete result.init
  return result
}

const mockChangeCal = jest.fn((input) => handleChangeCalculate(input))

describe('the change calculation', () => {
  test('input is 0', () => {
    expect(mockChangeCal(0)).toEqual(
      {
        '1': 0,
        '5': 0,
        '10': 0,
        '20': 0,
        '50': 0,
        '100': 0,
        '0.25': 0,
        '0.1': 0,
        '0.05': 0,
        '0.01': 0,
      }
    );
  });
  test('input is 1', () => {
    expect(mockChangeCal(1)).toEqual(
      {
        '1': 1,
        '5': 0,
        '10': 0,
        '20': 0,
        '50': 0,
        '100': 0,
        '0.25': 0,
        '0.1': 0,
        '0.05': 0,
        '0.01': 0,
      }
    );
  });
  test('input is .99', () => {
    expect(mockChangeCal(.99)).toEqual(
      {
        '1': 0,
        '5': 0,
        '10': 0,
        '20': 0,
        '50': 0,
        '100': 0,
        '0.25': 3,
        '0.1': 2,
        '0.05': 0,
        '0.01': 4,
      }
    );
  });
  test('input is 124.67', () => {
    expect(mockChangeCal(124.67)).toEqual(
      {
        '1': 4,
        '5': 0,
        '10': 0,
        '20': 1,
        '50': 0,
        '100': 1,
        '0.25': 2,
        '0.1': 1,
        '0.05': 1,
        '0.01': 2,
      }
    );
  });
});