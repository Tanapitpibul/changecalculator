import React, { Component } from 'react';
import styled, { keyframes } from 'styled-components'
import _ from 'lodash'

import './App.css';

const KeyFrameSubmitButton = keyframes`
  0% {
    background-position: 0%;
  }
  100% {
    background-position: 400%;
  }
`
const FlexForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`
const Label = styled.label`
  margin-bottom: 10px;
`
const Input = styled.input`
  padding: 5px 10px;
  height: 20px;
  width: 260px;
  border-radius: 20px;
  border-width: 0;
  margin-right: 10px;
  text-align: center;
  &:focus {
    outline: -webkit-focus-ring-color auto 0px;
  }
`
const SubmitButton = styled.input`
  padding: 5px 10px;
  height: 40px;
  width: 100px;
  border-radius: 20px;
  border-width: 0;
  color: #fff;
  background: linear-gradient(90deg, #03a9f4, #ffeb3b, #f441a5, #03a9f4);
  background-position: 100%;
  background-size: 400%;
  box-sizing: border-box;
  &:hover {
    outline: -webkit-focus-ring-color auto 0px;
    animation: ${KeyFrameSubmitButton} 8s linear infinite;
  }
  &:focus {
    outline: -webkit-focus-ring-color auto 0px;
  }
  &:active {
    outline: -webkit-focus-ring-color auto 0px;
    opacity: 0.8;
  }
`
const Text = styled.p`
  height: 50px;
  margin: 0px;
  margin-top: 10px;
  font-size: 2rem;
`
const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
const array = [100, 50, 20, 10, 5, 1, .25, .10, .05, .01]

const reduce = (result,currentValue) => {
  result[currentValue] = Math.floor(result.init / currentValue)
  result.init = ((result.init * 100) % (currentValue * 100)) / 100
	return result
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      changeList: null,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({value: event.target.value});
  }
  handleSubmit(event) {
    const { value } = this.state
    const changeList = this.handleChangeCalculate(value)
    this.setState({ changeList })
    event.preventDefault();
  }
  handleChangeCalculate = (param) => {
    const result = array.reduce(reduce, { init: param })
    delete result.init
    return result
  }
  singularOrPlural = (value, type) => {
    if(value > 1) {
      if(type === 'penny') {
        return 'ies'
      } else {
        return 's'
      }
    } else {
      if(type === 'penny') {
        return 'y'
      } else {
        return ''
      }
    }
  }
  textChangeCalculated = (changeList) => {
    return _.compact([
      changeList[100] ? `${changeList[100]} 100 dollar bill${this.singularOrPlural(changeList[100])}` : '',
      changeList[50] ? `${changeList[50]} 50 dollar bill${this.singularOrPlural(changeList[50])}` : '',
      changeList[20] ? `${changeList[20]} 20 dollar bill${this.singularOrPlural(changeList[20])}` : '',
      changeList[10] ? `${changeList[10]} 10 dollar bill${this.singularOrPlural(changeList[10])}` : '',
      changeList[5] ? `${changeList[5]} 5 dollar bill${this.singularOrPlural(changeList[5])}` : '',
      changeList[1] ? `${changeList[1]} 1 dollar bill${this.singularOrPlural(changeList[1])}` : '',
      changeList[0.25] ? `${changeList[0.25]} quarter${this.singularOrPlural(changeList[0.25])}` : '',
      changeList[0.1] ? `${changeList[0.1]} dime${this.singularOrPlural(changeList[0.1])}` : '',
      changeList[0.05] ? `${changeList[0.05]} nickle${this.singularOrPlural(changeList[0.05])}` : '',
      changeList[0.01] ? `${changeList[0.01]} penn${this.singularOrPlural(changeList[0.01], 'penny')}` : '',
    ]).join(', ')
  }
  render() {
    const { value, changeList } = this.state
    let changeResult = ''
    if(changeList) {
      changeResult = this.textChangeCalculated(changeList)
    }
    return (
       <div className="App">
         <header className="App-header">
          <FlexForm onSubmit={this.handleSubmit}>
            <Label>
              Please enter a change in dollars
            </Label>
            <FlexRow>
              <Input
                type="number"
                min={0}
                step="0.01"
                precision={2}
                value={value}
                onChange={this.handleChange}
              />
              <SubmitButton type="submit" value="Calculate" />
            </FlexRow>
          </FlexForm>
          {
            <Text>{changeResult !== '' && `Your change is ${changeResult}`}</Text>
          }
         </header>
       </div>
    );
  }
}

export default App;
