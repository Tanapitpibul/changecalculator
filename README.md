# changeCalculator

A change calculator that when given an argument of dollars and cents, output the smallest quantity of bills and coins equal to the amount.

# How to clone the project

- run `git clone https://gitlab.com/Tanapitpibul/changecalculator.git`

## How to run

- run `npm install` or `yarn` to install packages
- run `npm start` or `yarn start` to render screen in localhost:3000

## How to test

I store the test in src/App.test.js
- run `npm test`